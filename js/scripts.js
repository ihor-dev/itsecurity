
$(document).ready(function(){

	var $menu = $(".fmenu");
		 
	$(window).scroll(function(){

		if ( $(this).scrollTop() > 2 && $menu.hasClass("default")){
			$menu.fadeOut('fast',function(){
				$(this).removeClass("default")
					.addClass("fixed-menu")
					.fadeIn('fast');
			});
		} else if($(this).scrollTop() <= 2 && $menu.hasClass("fixed-menu")) {
			$menu.fadeOut('fast',function(){
				$(this).removeClass("fixed-menu")
					.addClass('default')
					.fadeIn('fast');
			});
		}
	});//scroll
});
