function getCoordinate(data) {

    var option = {
        'xPrev': 0,
        'yPrev': 0,
        'xPos': 0, // L,0,R
        'yPos': 0, // T,0,B
        'mouseport': data.mouseport,
        'obj': data.obj,
        'ratioX': parseFloat(data.ratioX),
        'ratioY': parseFloat(data.ratioY),

    }
    var o = option;
    
    //set default position
    $(o.obj).attr('x', $(o.obj).offset().left);
    $(o.obj).attr('y', $(o.obj).offset().top);
        
    //return to dafault position    
    $(o.mouseport).mouseleave(function(){
        $(o.obj).animate({
            'left' : '0',
            'top' : 0
        },600);
    });


    console.log(o.mouseport);
    //if mousemove
    $(o.mouseport).mousemove(function (e) {
		
        if(o.xPrev < e.pageX){o.xPos = 'R';}
        else if(o.xPrev > e.pageX) {o.xPos = 'L';}
        else if(o.xPrev == e.pageX) {o.xPos = '0';}
        o.xPrev = e.pageX;

        if(o.yPrev < e.pageY){o.yPos = 'B';}
        else if(o.yPrev > e.pageY) {o.yPos = 'T';}
        else if(o.yPrev == e.pageY) {o.yPos = '0';}
        o.xPrev = e.pageX;
        
        o.yPrev < e.pageY ? o.yPos = 'B' : o.yPos = 'T';
        o.yPrev = e.pageY;

        /* develop*/
        //do Y-function
        $('#goTo').html('Y move to: ' +o.yPos);


        //do X-function
        $('#lr').html('X move to: ' +o.xPos);
           
        //page coordinates
        $('#status').html('Coordinate of page (X,Y): '+e.pageX+', '+e.pageY);    
        
        

        //move object
        if(o.xPos =='L' && o.yPos =='B'){
            $(o.obj).offset(function(i, coord){
                var  newCoord = {};                
                newCoord.left = coord.left - 1 * o.ratioX;
                newCoord.top = coord.top + 1 * o.ratioY;
                return newCoord;
            });                
        }else if(o.xPos =='L' && o.yPos =='T'){
             $(o.obj).offset(function(i, coord){
                var  newCoord = {};                
                newCoord.left = coord.left - 1 * o.ratioX;
                newCoord.top = coord.top - 1 * o.ratioY;
                return newCoord;
            });
        }else if(o.xPos =='R' && o.yPos =='B'){
             $(o.obj).offset(function(i, coord){
                var  newCoord = {};                
                newCoord.left = coord.left + 1 * o.ratioX;
                newCoord.top = coord.top + 1 * o.ratioY;
                return newCoord;
            });
        }else if(o.xPos =='R' && o.yPos =='T'){
             $(o.obj).offset(function(i, coord){
                var  newCoord = {};                
                newCoord.left = coord.left + 1 * o.ratioX;
                newCoord.top = coord.top - 1 * o.ratioY;
                return newCoord;
            });
        }            
    });

}
